define("common/mask.js", ['jquery', 'common/spin.js'], function(e, t, n) {
    "use strict";

    var $ = e('jquery');
    function r(e) {
        if (this.mask) this.mask.show();
        else {
            var t = "body";
            e && !! e.parent && (t = $(e.parent)), this.mask = $(s).appendTo(t), this.mask.id = "wxMask_" + ++i;
        }
        if (e) {
            if (e.spin === !1) return this;
            this.mask.spin(e.spin || "flower");
        }
        return this;
    }
    e("common/spin.js");
    var i = 0,
        s = '<div class="mask"></div>';
    r.prototype = {
        show: function() {
            this.mask.show();
        },
        hide: function() {
            this.mask.hide();
        },
        remove: function() {
            this.mask.remove();
        }
    }, t.show = function(e) {
        return new r(e);
    }, t.hide = function() {
        $(".mask").hide();
    }, t.remove = function() {
        $(".mask").remove();
    };
});