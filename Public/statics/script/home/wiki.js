define(function(require, exports, module) {
    var $ = require('jquery');
    require('jquery/plugins/jquery.cookie.js');
    //require('jquery/plugins/jquery.md5');
    //require('jquery/plugins/jquery.validate');
    //var verify = require('common/verifycode');
    //var msg = require('common/msgtip');

    //var Handlebars = require('handlebars');
    var Handlebars = require('handlebars-helper');

    //全局执行函数
    var global = require('home/global.js');

    ////$(function () {

        //进行数据处理、html构造
        //注册一个Handlebars模版，通过id找到某一个模版，获取模版的html框架
        var myTemplate = Handlebars.compile($("#entry-template").html());
        //将json对象用刚刚注册的Handlebars模版封装，得到最终的html，插入到基础table中。
        $('#resMenu').html(myTemplate(result.data));

        //显示菜单
        showMenu();
    ////});

    //读取cookie
    function getCookie(name) {
        if($.cookie) {
            return $.cookie(name)
        }
        else {
            var module = seajs.cache[seajs.resolve("home/global.js")];
            if(!module){
                global = require('home/global.js');
            }
            return global.getCookie(name);
        }
    }

    //设置cookie
    function setCookie(name, value, days) {
        if($.cookie) {
            var r = new Date();
            if(days==null || days==0) days=1;
            r.setTime(r.getTime() + 3600000*24*days);
            //$.cookie(name, decodeURIComponent(value), { expires: r });
            $.cookie(name, value, { expires: r });
        }
        else {
            var module = seajs.cache[seajs.resolve("home/global.js")];
            if(!module){
                global = require('home/global.js');
            }
            global.setCookie(name, value, days);
        }
    }

    //顶级菜单 展开折叠
    $('body').on('click', '.menu_title_primary', function () {
        $(this).toggleClass('closed');
        if ($(this).is('.closed')) {
            $(this).next('.menu_desc_primary').css('display', 'none');
        }
        else {
            $(this).next('.menu_desc_primary').css('display', 'block');
        }
    });
    //子级菜单 展开折叠
    $('body').on('click', '.sub_menu_title_primary', function () {
        $(this).toggleClass('closed');
        if ($(this).is('.closed')) {
            $(this).next('.sub_menu_desc_primary').css('display', 'none');
        }
        else {
            $(this).next('.sub_menu_desc_primary').css('display', 'block');
        }
    });
    //点击超链接
    $('body').on('click', '.menu_link_primary', function () {
        var n = $(this).attr('data-id');
        showMenu(n);
    });
    function showMenu(n) {
        var hash = location.hash ? location.hash.replace('#','') : '';
        if(!hash) {
            hash = getCookie('hash');
        }
        hash = hash ? hash : '';
        setCookie('hash', hash);
        var menuid = '';
        if(hash.indexOf('view-') === 0) {
            menuid = hash.replace('view-','').split('-');
            if(menuid.length==0) {
                menuid = '';
            }
        }
        if(!n && menuid) {
            n = menuid[menuid.length-1];
        }
        n = n || '';
        if(!n) {
            //return;
            n = $('.sub_menu_title_primary').children('a').attr('data-id');
        }
        //隐藏项字体颜色变灰
        $('[data-hidden=1] a').filter('.menu_link_primary').css('color', '#999999');
        //高亮选中
        $('.menu_link_primary').removeClass('selected');
        var dom = $('.menu_link_primary').filter('[data-id='+n+']');
        if(dom.length==0) return;
        dom.addClass('selected');
        //展开选项
        if(dom.parents('dd').is('.sub_menu_desc_primary')) {
            //三级菜单
            var f = dom.parents('.sub_menu_desc_primary');
            f.css('display', 'block');
            f.prev('.sub_menu_title_primary').removeClass('closed');
            f.parents('.menu_desc_primary').css('display', 'block');
            f.parents('.menu_desc_primary').prev('.menu_title_primary').removeClass('closed');
            f.parents('.menu_desc_primary').parents('.menu_desc_primary').css('display', 'block');
            f.parents('.menu_desc_primary').parents('.menu_desc_primary').prev('.menu_title_primary').removeClass('closed');
        }
        else {
            //两级菜单
            var f = dom.parents('.menu_desc_primary');
            f.css('display', 'block');
            f.prev('.menu_title_primary').removeClass('closed');
        }
        var url = dom.attr('data-url');
        if(url) {
            $('#iframe',document.body).attr('src', url);
            $("#iframe").load(function(){
				try {
					var mainheight = $(this).contents().find("body").height()+60;
					$(this).height(mainheight);
				} catch(e) {
					var mainheight = $(window.frames["iframe"].document).find("body").height()+60;
					$(this).height(mainheight);
				}
            });
        }
    }

});


