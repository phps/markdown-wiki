<?php
namespace Home\Controller;
class WikiController extends BaseController{

    private $_mod;
    private $wiki_dir;
    private $cacheKey;

    public function _initialize() {
        parent::_initialize();
        //数据模型
        $this->_mod = M('Wiki');
        //wiki文档目录
        $this->wiki_dir = C('WIKI_DIR') ? C('WIKI_DIR') : 'wiki';
        //缓存数据key
        $this->cacheKey = C('WIKI_CACHE_KEY') ? C('WIKI_CACHE_KEY') : 'wiki_preview_';
    }

    /**
     * Wiki首页
     */
    public function index()
    {
        $sys['title'] = '资源中心 - 开发平台开发者文档';
        $sys['name'] = C('WEBNAME');
        $sys['webname'] = '开发平台开发者文档';
        $sys['subname'] = '欢迎进入开发者文档中心';
        $this->assign('sys', $sys);
        //显示菜单
        $this->wikiMenu();
        $nav = $this->_mod->where(array('title'=>'首页','pid'=>0))->find();
        $this->assign('nav', $nav);
        $content = '';
        if($nav['filename']) {
            $filepath = dirname(APP_PATH).'/'.$this->wiki_dir.'/'.$nav['filename'];
            if(IS_WIN) {
                $filepath = iconv("UTF-8", "GB2312", $filepath);
            }
            $content = file_get_contents($filepath);
            $content = parseMarkdown($content);
        }
        //$content = str_replace('language-', '', $content);
        $this->assign('content', $content);
        //页面css
        $this->assign('pagecss', 'home/wiki.css');
        $this->display();
    }

    public function view()
    {
        $aid = I('aid',0,'intval');
        $nav = $this->_mod->find($aid);
        $this->assign('nav', $nav);
        $content = '';
        if($nav['filename']) {
            $filepath = dirname(APP_PATH).'/'.$this->wiki_dir.'/'.$nav['filename'];
            if(IS_WIN) {
                $filepath = iconv("UTF-8", "GB2312", $filepath);
            }
            $content = file_get_contents($filepath);
            $content = parseMarkdown($content);
        }
        //$content = str_replace('language-', '', $content);
        $this->assign('content', $content);
        //$this->display('view(20150321)');
        $this->display();
    }

    //markdown预览
    public function preview()
    {
        $aid = I('aid',0,'intval');
        $title = $content = '';
        if($aid) {
            $res = $this->_mod->find($aid);
            $filepath = dirname(APP_PATH).'/'.$this->wiki_dir.'/'.$res['filename'];
            if(IS_WIN) {
                $filepath = iconv("UTF-8", "GB2312", $filepath);
            }
            $content = file_get_contents($filepath);
            $content = parseMarkdown($content);
            $title = $res['title'];
        }
        //$content = str_replace('language-', '', $content);
        $this->assign('content', $content);
        $this->assign('title', $title);
        //取出缓存预览数据
        $cache_title = session($this->cacheKey.'wiki_preview_title');
        $cache_content = session($this->cacheKey.'wiki_preview_content');
        $this->assign('cache_title', $cache_title);
        $this->assign('cache_content', $cache_content);
        $this->display();
    }

    /**
     * 获取菜单
     */
    public function menu() {
        layout(false);
        //获取菜单
        $this->wikiMenu();
        $this->display();
    }

    //获取markdown解析信息
    public function markdown()
    {
        $title = I('title','','trim');
        $content = I('content','','trim');
        $content = parseMarkdown($content);
        $result = array(
            'title'=>$title,
            'content'=>$content,
        );
        echo json_encode($result);
        exit;
    }

    //获取左侧菜单
    public function wikiMenu() {
        $preview = I('preview',0,'intval');
        if(!$preview) {
            $map['status'] = 1;
        }
        $data = $this->_mod->where($map)->order('list_order ASC, aid ASC')->select();
        $menu = \Common\Lib\Tool\Data::channelLevel($this->addNodeUrl($data), 0, '&nbsp;', 'aid');
        $this->assign('menu', $menu);
        $node = $this->getWikiNode($menu);
        $this->assign('node', $node);
    }

    /**
     * 获得节点菜单
     * @param $node
     * @return mixed
     */
    protected function addNodeUrl($node)
    {
        if (empty($node)) {
            return array();
        }
        //设置链接
        foreach ($node as $n => $v) {
            //$node[$n]['url'] = U('Index/view',array('aid'=>$v['aid']));
            $node[$n]['url'] = __ROOT__.'/Home/Wiki/view/aid/'. $v['aid'] .'.html';
        }
        return $node;
    }

    protected function getWikiNode($menu)
    {
        $result = array();
        foreach($menu as $key=>$node) {
            $rs = array(
                'id' => $node['aid'],
                'name' => $node['title'],
                'url' => $node['url'],
                'leaf' => $node['_data'] ? 0 : 1,
                'parent' => $node['pid'],
                'hidden' => $node['status'] ? 0 : 1, //显示隐藏项
                'children' => $node['_data'] ? $this->getWikiNode($node['_data']) : [],
            );
            $result[] = $rs;
        }
        return $result;
    }

}
