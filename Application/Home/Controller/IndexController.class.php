<?php
namespace Home\Controller;
class IndexController extends BaseController {

    //构造函数
    public function _initialize()
    {
        parent::_initialize();
    }

    public function index(){
        //页面css
        $this->assign('pagecss', 'home/index.css');
        $this->display();
    }

}