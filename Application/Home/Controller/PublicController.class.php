<?php
namespace Home\Controller;
//use Home\Service\Process;
class PublicController extends BaseController {

    protected $_AreaMod;

    //构造函数
    public function _initialize()
    {
        parent::_initialize();
        $this->_AreaMod = D('Area');
    }

    public function index(){
        exit('Access Denied');
    }

    //生成  验证码 图片的方法
    public function verify() {
        //3.2.1  中的生成 验证码 图片的方法
        $Verify = new \Think\Verify();
        // 设置验证码字符为纯数字
        $Verify->codeSet = '0123456789';
        $Verify->length   = 4;
        $Verify->entry();
    }

    //下拉区域数据
    public function getareas() {
        $id = I('id', 0, 'trim');
        $data = array();
        $map = array('parentid'=>$id);
        $num = $this->_AreaMod->where($map)->count();
        $list = $this->_AreaMod->where($map)->order('sort asc, id asc')->select();
        foreach($list as $key=>$row) {
            $area_name = $row['shortname'] ? $row['shortname'] : $row['areaname'];
            $area_name = str_replace(array('省', '市'), '', $area_name);
            $data[] = array(
                'id' => $row['id'],
                'name'=> $area_name,
            );
        }
        $result = array(
            'num'=>$num,
            'data'=>$data,
        );
        header('Content-Type:application/json; charset=utf-8');
        echo(json_encode($result, JSON_UNESCAPED_UNICODE));
        exit();
    }
}