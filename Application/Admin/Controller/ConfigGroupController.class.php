<?php
namespace Admin\Controller;
class ConfigGroupController extends BaseController{

    private $_mod;

    public function _initialize() {
        parent::_initialize();
        $this->_mod = D('ConfigGroup');
    }

    //修改网站配置(基本配置）
    public function index()
    {
        //获取组列表
        $data = $this->_mod->getGroup(1);
        $this->assign('data', $data);
        $this->display();
    }

    /**
     * 添加组
     */
    public function add()
    {
        if (IS_POST) {
            if ($this->_mod->addConfigGroup()) {
                $this->success('添加成功');
            } else {
                $this->error($this->_mod->getError());
            }
        } else {
            $this->assign('res',null);
            $this->display('edit');
        }
    }

    //修改组
    public function edit()
    {
        if (IS_POST) {
            if ($this->_mod->editConfigGroup()) {
                $this->success('修改成功', U('index'));
            } else {
                $this->error($this->_mod->getError());
            }
        } else {
            $cgid = I('cgid', 0, 'intval');
            $res = $this->_mod->find($cgid);
            if (!$res) {
                $this->error($this->_mod->getError());
            } else {
                $this->assign("res", $res);
                $this->display();
            }
        }
    }

    /**
     * 删除配置组
     */
    public function del()
    {
        if ($this->_mod->delConfigGroup()) {
            $this->success('删除成功');
        } else {
            $this->error($this->_mod->getError());
        }
    }
}