<?php
namespace Admin\Controller;
class AdministratorController extends BaseController{

    private $_mod;
    private $_roleMod;
    private $_userTable = "user";

    public function _initialize() {
        parent::_initialize();
        $this->_mod = D('User');
        $this->_roleMod = D("Role");
    }

    /**
     * 管理员列表
     */
    public function index()
    {
        $map['admin'] = 1;
        $count = $this->_mod->where($map)->count();
        if (! empty ( $_REQUEST ['pagesize'] )) {
            $listRows = $_REQUEST ['pagesize'];
        } else {
            $listRows = C('PAGE_SIZE') ? C('PAGE_SIZE') : 10;
        }
        $page = $this->page($count, $listRows);
        $data = $this->_mod->where($map)->limit($page->firstRow . ',' . $page->listRows)->order("uid ASC")->select();
        $this->assign("data", $data);
        $this->assign('page', $page->show());
        $this->display();
    }

    //删除管理员
    public function del()
    {
        $uid = I("post.uid", 0, "trim");
        if ($this->_mod->delUser($uid)) {
            $this->success('删除成功');
        }
    }

    //添加管理员
    public function add()
    {
        if (IS_POST) {
            if ($this->_mod->addUser()) {
                $this->success("添加成功！");
            } else {
                $this->error($this->_mod->getError());
            }
        } else {
            $role = $this->_roleMod->where('admin=1')->order("rid DESC")->select();
            $this->assign('role',$role);
            $this->display('edit');
        }
    }

    //修改管理员
    public function edit()
    {
        if (IS_POST) {
            $uid = I('uid', 0, 'intval');
            $_POST['uid'] = $uid;
            if ($this->_mod->editUser($_POST)) {
                $this->success("修改成功！");
            } else {
                $this->error($this->_mod->getError());
            }
        } else {
            $uid = I("request.uid", null, "intval");
            if ($uid) {
                $res = $this->_mod->where(array('uid'=>$uid))->find();
                $role = $this->_roleMod->where('admin=1')->order("rid DESC")->select();
                $this->assign('res',$res);
                $this->assign('role',$role);
                $this->display();
            }
        }
    }

    /**
     * 设置状态
     * @return string
     */
    public function setStatus()
    {
        $id = I('id',0,'intval');
        if($id != 1) {
            parent::setStatus( M($this->_userTable) );
        }
    }
}