<?php
namespace Admin\Controller;
class WikiController extends BaseController{

    private $_mod;
    private $node; //节点树
    private $map;
    private $cacheKey; //缓存数据key

    public function _initialize()
    {
        parent::_initialize();
        $this->_mod = D('Wiki');
        $this->map = array();
        //$this->map['status'] = 1;
        $data = $this->_mod->where($this->map)->order('list_order ASC, aid ASC')->select();
        $node = \Common\Lib\Tool\Data::tree($data, "title", "aid", "pid");
        $this->node = $node;
        $this->cacheKey = C('WIKI_CACHE_KEY') ? C('WIKI_CACHE_KEY') : 'wiki_preview_';
    }

    //节点列表
    public function index()
    {
        $result = $this->_mod->field('aid, title, pid, status, type')->where($this->map)->order('list_order ASC, aid ASC')->select();
        $json = array();
        $json[] = array(
            'aid' => '0',
            'pid' => '0',
            'title' => '根节点',
            'open' => true,
            'status' => 1,
        );
        foreach ($result as $n => $rs) {
            $count = $this->_mod->where(array_merge(array('pid'=>$rs['aid']), $this->map))->count();
            $isParent = ($count>0) ? true : false;
            $data = array(
                'aid' => $rs['aid'],
                'pid' => $rs['pid'],
                'title' => $rs['title'],
                'isParent' => $isParent,
                'status' => (int)$rs['status'],
                //'checked' => false,
                //'chkDisabled' => false,
            );
            $data['loadurl'] = U('Wiki/editwiki',array('aid'=>$rs['aid']));
            if(!$isParent) {
                //// $data['url'] = U('Wiki/edit',array('aid'=>$rs['aid']));
                //// $data['target'] = "right";
            }
            $json[] = $data;
        }
        $this->assign('json', $json);
        $this->display();
    }

    //wiki编辑
    public function editwiki()
    {
        $aid = I('aid', 0, 'intval');
        if(IS_POST) {
            $content = I('content');
            $content = str_replace(array('&lt;','&gt;'), array('<', '>'), $content);
            $data = array(
                'aid'=>$aid,
                'title'=>I('title','','trim'),
                'filename'=>I('filename','','trim'),
                'content'=>$content,
                'list_order'=>I('list_order',0,'intval'),
                'status'=>I('status',0,'intval'),
            );
            $r = $this->_mod->saveWiki($data);
            if(!$r) {
                $error = $this->_mod->getError() ? $this->_mod->getError() : '操作失败';
                $this->error($error);
            }
            else {
                $this->success('文档保存成功！');
            }
        }
        else {
            $rs = $this->_mod->find($aid);
            $filename = $rs['filename'];
            $readfile = $this->_mod->wiki_path.$filename;
            $readfile = $this->_mod->getFilePath($readfile);
            $content = @file_get_contents($readfile);
            $content = str_replace(array('&lt;','&gt;'), array('<', '>'), $content);
            $res = array(
                'aid' => $aid,
                'title' => $rs['title'],
                'list_order' => $rs['list_order'],
                'filename' => $filename,
                'content' => $content,
                'status' => (int)$rs['status'],
            );
            $this->assign('res', $res);
            $this->display();
        }
    }

    //重命名
    public function rename()
    {
        $aid = I('aid', 0, 'intval');
        $title = I('title', '', 'trim');
        $data = array(
            'aid'=>$aid,
            'title'=>$title
        );
        $oldTitle = $this->_mod->where(array('aid'=>$aid))->getField('title');
        $r = $this->_mod->rename($data);
        if(!$r) {
            $error = $this->_mod->getError() ? $this->_mod->getError() : '操作失败';
            $this->error($error);
        }
        else {
            $title = $title == $oldTitle ? '' : '标题重命名成功！';
            $this->success($title);
        }
    }

    //移动
    public function move()
    {
        $aid = I('aid', '', 'trim');
        $pid = I('pid', 0, 'intval');
        $data = array(
            'aid'=>$aid,
            'pid'=>$pid,
        );
        $r = $this->_mod->move($data);
        if(!$r) {
            $error = $this->_mod->getError() ? $this->_mod->getError() : '操作失败';
            $this->error($error);
        }
        else {
            $this->success('文档移动成功！');
        }
    }

    //删除
    public function del()
    {
        $aid = I('aid', '', 'trim');
        $data = array(
            'aid'=>$aid,
        );
        $r = $this->_mod->del($data);
        if(!$r) {
            $error = $this->_mod->getError() ? $this->_mod->getError() : '操作失败';
            $this->error($error);
        }
        else {
            $this->success('文档删除成功！');
        }
    }

    //添加
    public function add()
    {
        if(IS_POST) {
            $r = $this->_mod->addWiki($_POST);
            if(!$r) {
                $error = $this->_mod->getError() ? $this->_mod->getError() : '操作失败';
                $this->error($error);
            }
            else {
                $this->success('文档添加成功！');
            }
        }
        else {
            $this->assign('node', $this->node);
            $this->display();
        }
    }

    //ajax添加文档节点
    public function addnew()
    {
        $pid = I('pid', 0, 'intval');
        $title = I('title', '', 'trim');
        $title = $title ? $title : '新建文档';
        $status = 0;
        $data = array(
            'pid'=>$pid,
            'title'=>$title,
            'status'=>$status
        );
        $r = $this->_mod->addNew($data);
        if(!$r) {
            $error = $this->_mod->getError() ? $this->_mod->getError() : '操作失败';
            $this->error($error);
        }
        else {
            $result = array(
                'aid' => $r,
                'title' => $title,
            );
            $this->success('文档添加成功！', '', $result);
        }
    }

    //文档预览数据缓存
    public function preview_cache()
    {
        $title = I('title','','trim');
        $content = I('content','','trim');
        //缓存数据：
        session($this->cacheKey.'wiki_preview_title', $title);
        session($this->cacheKey.'wiki_preview_content', $content);
        exit('ok');
    }

}