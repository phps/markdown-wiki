<?php
namespace Admin\Controller;
class ConfigController extends BaseController{

    private $_mod;

    public function _initialize() {
        parent::_initialize();
        $this->_mod = D('Config');
    }

    //修改网站配置(基本配置）
    public function index()
    {
        if (IS_POST) {
            if ($this->_mod->editCfg()) {
                $this->success("修改成功");
            } else {
                $this->error($this->_mod->getError());
            }
        } else {
            //配置信息
            $data = $this->_mod->getCfg();
            $this->assign('data', $data);
            //配置组
            $configGroup = $this->_mod->getCfgGroup();
            $this->assign("configGroup", $configGroup);
            $this->display();
        }
    }

    //添加配置项
    public function add()
    {
        if (IS_POST) {
            if ($this->_mod->addCfg()) {
                $this->success('添加成功!', U('index'));
            } else {
                $this->error($this->_mod->getError());
            }
        } else {
            $configGroup = $this->_mod->getCfgGroup();
            $this->assign("configGroup", $configGroup);
            $this->display();
        }
    }

    //删除配置
    public function del()
    {
        if ($this->_mod->delCfg()) {
            $this->success('操作成功');
        } else {
            $this->error($this->_mod->getError());
        }
    }

    //更新缓存
    public function updateCache()
    {
        if ($this->_mod->updateCache()) {
            $this->success('缓存更新成功！');
        } else {
            $this->error($this->_mod->getError());
        }
    }

    //ajax测试发送邮件
    public function ajaxmail()
    {
        if(IS_POST) {
            $mail_to = I('mail_to','','trim');
            if(empty($mail_to)) {
                $this->error('请输入测试邮件！');
                exit;
            }
            $config = array(
                'mail_host' => I('mail_host','','trim'), //邮件服务器
                'mail_port' => I('mail_port','','trim'), //邮件发送端口
                'mail_from' => I('mail_from','','trim'), //发件人地址
                'mail_fname' => I('mail_fname','','trim'), //发件人名称
                'mail_user' => I('mail_user','','trim'), //验证登陆名
                'mail_pwd' => I('mail_pwd','','trim'), //验证密码
            );
            $r = SendMail($mail_to, '测试发送邮件', '邮件发送内容', $config);
            if($r) {
                $this->success('测试邮件发送成功！');
            }
            else {
                $this->error('发送失败，请检查配置信息！');
            }
        }
        else {
            $this->error('系统错误');
        }
    }

}
