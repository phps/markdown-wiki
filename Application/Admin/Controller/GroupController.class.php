<?php
namespace Admin\Controller;
class GroupController extends BaseController{

    //模型
    private $_mod;
    private $_roleMod;

    //构造函数
    public function _initialize()
    {
        parent::_initialize();
        $this->_mod = D('Group');
        $this->_roleMod = D("Role");
    }

    //会员组列表
    public function index()
    {
        $data = $this->_roleMod->where('admin=0')->order('rid ASC')->select();
        $this->assign('data', $data);
        $this->display();
    }

    /**
     * 添加会员组
     */
    public function add()
    {
        if (IS_POST) {
            if ($aid = $this->_mod->addRole()) {
                $this->success('添加成功');
            } else {
                $this->error($this->_mod->getError());
            }
        } else {
            $this->assign('res', null);
            $this->display('edit');
        }
    }

    //编辑会员组
    public function edit()
    {
        if (IS_POST) {
            if ($this->_mod->editRole()) {
                $this->success('修改成功');
            } else {
                $this->error($this->_mod->getError());
            }
        } else {
            $rid = I('rid', 0, 'intval');
            $res = $this->_roleMod->find($rid);
            $this->assign('res', $res);
            $this->display();
        }
    }

    //删除会员组
    public function del()
    {
        if ($this->_mod->delRole()) {
            $this->success('删除成功');
        } else {
            $this->error($this->_mod->getError());
        }
    }
}