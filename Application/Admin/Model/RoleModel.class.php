<?php
namespace Admin\Model;
class RoleModel extends \Think\Model{

    //表名
    public $tableName = 'role';
    public $userTable = "user";

    //验证
    protected $_validate = array(
        //array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
        array('rname', 'require', '角色名不能为空'),
        array('rname','','角色已经存在', 0, 'unique', 3),
    );

    //添加角色
    public function addRole()
    {
        if ($this->create()) {
            if ($this->add()) {
                return true;
            } else {
                $this->error = '添加失败';
            }
        }
    }

    //添加角色
    public function editRole()
    {
        if ($this->create()) {
            if (false !== $this->save()) {
                return true;
            } else {
                $this->error = '修改失败';
            }
        }
    }

    //删除角色
    public function delRole()
    {
        $rid = I('rid', 0, 'trim');
        if ($this->delete($rid)) {
            M($this->userTable)->where(array( 'rid' => ['IN',explode(',',$rid)] ))->save(array('rid' => 4));
            return true;
        } else {
            $this->error = '删除失败';
        }
    }

}