<?php
namespace Admin\Model;
class PanelModel extends \Think\Model{

    //表名
    public $tableName = 'panel';
    public $nodeTable = "node";

    //设置快捷面板
    public function setPanel($uid)
    {
        if($uid) {
            //删除旧面板
            $map['uid'] = $uid;
            $this->where($map)->delete();
            if (!empty($_POST['nid'])) {
                foreach ($_POST['nid'] as $nid) {
                    $data=array(
                        'uid' => $uid,
                        'nid' => $nid
                    );
                    $this->add($data);
                }
            }
            return true;

        } else {
            $this->error = '参数错误';
        }
    }

    //获取常用菜单
    public function getPanel($uid)
    {
        $prefix = C('DB_PREFIX');
        $menu = array();
        if($uid) {
            $user = D('User')->getUserInfo((int) $uid);
            //超级管理员菜单
            if ($user['rid'] == 1) {
                $menu = M()->table("{$prefix}panel m")
                    ->field('n.*')
                    ->join("{$prefix}node n ON n.nid=m.nid", "RIGHT")
                    ->join("{$prefix}access a ON n.nid=a.nid", "LEFT")
                    ->where("n.is_show=1 AND m.uid={$uid}")
                    ->order('n.list_order ASC')
                    ->select();
            } else {
                $menu = M()->table("{$prefix}panel m")
                    ->field('n.*')
                    ->join("{$prefix}node n ON n.nid=m.nid", "RIGHT")
                    ->join("{$prefix}access a ON n.nid=a.nid", "LEFT")
                    ->where("n.is_show=1 AND (n.type=2 OR a.nid is not null) AND m.uid={$uid}")
                    ->order('n.list_order ASC')
                    ->select();
            }
        }
        return $menu;
    }

}