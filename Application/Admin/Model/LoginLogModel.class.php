<?php
namespace Admin\Model;
class LoginLogModel extends \Think\Model{

    public $tableName = 'login_log';

    //自动完成
    protected $_auto = array(
        //array(填充字段,填充内容,[填充条件,附加规则])
        array('logintime', 'time', 3, 'function'),
        array('loginip', 'get_client_ip', 3, 'function'),
    );

    //添加登录日志
    public function addLog($data = array())
    {
        if ($this->create($data)) {
            return $this->add() !== false ? true : false;
        }
        return false;
    }

    /**
     * 删除某个时间点前的日志
     * @return boolean
     */
    public function deleteLog($time = 0, $n = 1, $type = '') {
        $time = $time ? $time : time();
        $n = (int) $n;
        $map = array();
        //n天前
        if($type == 'd') {
            $map['logintime'] = array("lt", $time - (60 * 60 * 24) * $n);
        }
        //n小时前
        else if($type == 'h') {
            $map['logintime'] = array("lt", $time - (60 * 60) * $n);
        }
        //n分钟前
        else if($type == 'i') {
            $map['logintime'] = array("lt", $time - 60 * $n);
        }
        //n秒前
        else {
            $map['logintime'] = array("lt", $time * $n);
        }
        $status = $this->where($map)->delete();
        return $status !== false ? true : false;
    }

}