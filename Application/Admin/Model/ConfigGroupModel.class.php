<?php
namespace Admin\Model;
class ConfigGroupModel extends \Think\Model{

    //表名
    public $tableName = 'config_group';
    public $configTable = "config";

    //自动验证
    protected $_validate = array(
        //array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
        array('cgname', 'require', '组名不能为空'),
        array('cgname','','组名已经存在', 0, 'unique', 3),
        array('cgtitle', 'require', '组标题不能为空'),
        array('cgtitle','','组标题已经存在', 0, 'unique', 1),
    );

    /**
     * 获取组列表
     * @return mixed
     */
    public function getGroup()
    {
        return $this->where(array('isshow' => 1))->select();
    }

    /**
     * 添加配置组
     */
    public function addConfigGroup()
    {
        if ($this->create()) {
            if ($this->add()) {
                return true;
            } else {
                $this->error = '添加失败';
            }
        }
    }

    /**
     * 修改配置组
     */
    public function editConfigGroup()
    {
        if ($this->create()) {
            if (false !== $this->save()) {
                return true;
            } else {
                $this->error = '操作失败';
            }
        }
    }

    /**
     * 删除配置组
     * @return bool
     */
    public function delConfigGroup()
    {
        $cgid = I('cgid', 0, 'trim');
        $map['cgid'] = array('IN', explode(',',$cgid));
        if ($this->where($map)->delete()) {
            return M($this->configTable)->where($map)->delete();
        } else {
            $this->error('删除失败');
        }
    }
}