<?php
// 默认配置
$config = [
    // URL配置
    'URL_MODEL'             => 2,
	// URL区分大小写
    'URL_CASE_INSENSITIVE'  =>  false,
    // 调试配置
    'SHOW_PAGE_TRACE'       => 0, //显示调试信息

    //引入其他配置文件
    'LOAD_EXT_CONFIG'       => 'db,app',
    // 自动加载文件
    'LOAD_EXT_FILE'         => 'app', //自动导入函数文件
    // 自动加载目录
    'APP_AUTOLOAD_PATH'     => '@.TagLib',

    // Session自动开启
    'SESSION_AUTO_START'    => TRUE,
    'SESSION_OPTIONS'       =>  array(
        'name'              =>  'WIKISESSION',    //设置session名
        'expire'            =>  24*3600*1,        //SESSION保存1天
        'use_trans_sid'     =>  1,                //跨页传递
        'use_only_cookies'  =>  0,                //是否只开启基于cookies的session的会话方式
    ),

    // 分页配置
    'VAR_PAGE'              => 'page',
    // 过滤配置
    'VAR_FILTERS'           => 'htmlspecialchars',
];

return $config;
