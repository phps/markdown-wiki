<?php
/* 
 * 自定义全局函数
 */

//解析markdown文件内容为html文本
function parseMarkdown($text) {
    import('Parsedown');
    import('ParsedownExtra');
    $text = ParsedownExtra::instance()->parse($text);
    $text = str_replace(array('&amp;lt;','&amp;gt;'), array('&lt;','&gt;'), $text);
    //$text = str_replace(array('&quot;'), array('"'), $text);
    return $text;
}

/**
 * 邮件发送
 * @param string $address 接收人 单个直接邮箱地址，多个可以使用数组
 * @param string $title 邮件标题
 * @param string $message 邮件内容
 * @param array $config 配置
 * @return bool
 */
function SendMail($address, $title, $message, $config = array()) {
    $config = (array) $config;
    $base = array(
        'mail_host' => C('MAIL_HOST'), //邮件服务器
        'mail_port' => C('MAIL_PORT'), //邮件发送端口
        'mail_from' => C('MAIL_FROM'), //发件人地址
        'mail_fname' => C('MAIL_FNAME'), //发件人名称
        'mail_user' => C('MAIL_USER'), //验证登陆名
        'mail_pwd' => C('MAIL_PWD'), //验证密码
    );
    if(!$config) {
        $config = array_merge($base, (array) $config);
    }
    import('PHPMailer');
    try {
        $mail = new \PHPMailer();
        $mail->IsSMTP();
        // 设置邮件的字符编码，若不指定，则为'UTF-8'
        $mail->CharSet = C("DEFAULT_CHARSET");
        $mail->IsHTML(true);
        // 添加收件人地址，可以多次使用来添加多个收件人
        if (is_array($address)) {
            foreach ($address as $k => $v) {
                if (is_array($v)) {
                    $mail->AddAddress($v[0], $v[1]);
                } else {
                    $mail->AddAddress($v);
                }
            }
        } else {
            $mail->AddAddress($address);
        }
        // 设置邮件正文
        $mail->Body = $message;
        // 设置邮件头的From字段。
        $mail->From = $config['mail_from'];
        // 设置发件人名字
        $mail->FromName = $config['mail_fname'];
        // 设置邮件标题
        $mail->Subject = $title;
        // 设置SMTP服务器。
        $mail->Host = $config['mail_host'];
        // 设置为“需要验证”
        $mail->SMTPAuth = true;
        // 设置用户名和密码。
        $mail->Username = $config['mail_user'];
        $mail->Password = $config['mail_pwd'];
        return $mail->Send();
    } catch (phpmailerException $e) {
        return $e->errorMessage();
    }
}

/**
 * 验证手机号是否正确
 * @param int $mobile
 * @return boolean
 */
function isMobile($mobile) {
    if (!is_numeric($mobile)) {
        return false;
    }
    return preg_match("#^1[34578]\d{9}$#", $mobile) ? true : false;
    //return preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $mobile) ? true : false;
}

/**
 * 验证邮箱格式是否正确
 * @param int $email
 * @return boolean
 */
function isEmail($email) {
    return strpos($email, '@')>0 && filter_var($email, FILTER_VALIDATE_EMAIL);
}

/**
 * POST发送json数据
 * @param string $url
 * @param string $jsonstr
 * @return array
 */
function http_post_json($url, $jsonstr) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonstr);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Content-Length: ' . strlen($jsonstr))
    );
    ob_start();
    curl_exec($ch);
    $return_content = ob_get_contents();
    ob_end_clean();
    $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $result = array(
        'status'=>$return_code,
        'content'=>$return_content,
    );
    return $result;
}

/**
 * 获取提交的json串
 * @return array
 */
function fetch_post_json() {
    $contents = file_get_contents("php://input");
    $param = json_decode($contents, TRUE);
    return $param ? $param : array();
}
